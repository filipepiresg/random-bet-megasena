const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const numbers = [...Array(60).keys()].map((k) => k + 1);
const MIN_DEFAULT_NUMBER = 6;
const MAX_DEFAULT_NUMBER = 15;
const BET = [];

function getNumberBet(nCount = MIN_DEFAULT_NUMBER) {
  if (nCount >= 1) {
    const index_number = (Math.random() * (numbers.length - 1)).toFixed(0);

    BET.push(numbers[index_number]);
    numbers.splice(index_number, 1);

    getNumberBet(nCount - 1);
  } else {
    const _bet = [...BET];
    _bet.sort((a, b) => (a >= b ? 1 : -1));

    console.log(_bet);
  }
}

rl.question('Quantos números vc quer jogar? (MIN: 6, MAX: 15) ', (number = MIN_DEFAULT_NUMBER.toString()) => {
  let _number = isNaN(number) ? MIN_DEFAULT_NUMBER : number;

  if (_number > MAX_DEFAULT_NUMBER) {
    _number = MAX_DEFAULT_NUMBER;
  } else if (_number < MIN_DEFAULT_NUMBER) {
    _number = MIN_DEFAULT_NUMBER;
  }

  getNumberBet(_number);
  rl.close();
});

rl.on('close', () => {
  console.log('Boa sorte!!!');
  process.exit(0);
});
